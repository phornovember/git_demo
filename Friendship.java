package exercises;

public class Friendship {

	public static void main(String[] args) {
		loopPrintFriendShip(0,100);
		// B Task
		loopPrintFriendShip(20,30);
		// A Task
		loopPrintFriendShip(10,20);
	}
	
	// method do loop
	public static void loopPrintFriendShip(int start, int end){
		for (int i = start; i <= end; i++) {
			if (i % 3 == 0 && i % 5 == 0) {
				System.out.println(i + " - " + "Friendship");
			} else if (i % 5 == 0) {
				System.out.println(i + " - " + "Ship");
			} else if (i % 3 == 0) {
				System.out.println(i + " - " + "Friend");
			} else {
				System.out.println(i);
			}
		}
	}

}
